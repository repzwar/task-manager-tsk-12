package ru.pisarev.tm.repository;

import ru.pisarev.tm.api.ITaskRepository;
import ru.pisarev.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> list = new ArrayList<>();

    @Override
    public List<Task> findAll() {
        return list;
    }

    public Task findById(final String id) {
        for (Task task : list) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    public Task findByName(final String name) {
        for (Task task : list) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    public Task findByIndex(final int index) {
        return list.get(index);
    }

    @Override
    public void add(Task task) {
        list.add(task);
    }

    @Override
    public void remove(Task task) {
        list.remove(task);
    }

    public Task removeById(final String id) {
        final Task task = findById(id);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    public Task removeByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    public Task removeByIndex(final int index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    @Override
    public void clear() {
        list.clear();
    }

}

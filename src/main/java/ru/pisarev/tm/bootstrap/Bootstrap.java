package ru.pisarev.tm.bootstrap;

import ru.pisarev.tm.api.*;
import ru.pisarev.tm.constant.ArgumentConst;
import ru.pisarev.tm.constant.TerminalConst;
import ru.pisarev.tm.controller.CommandController;
import ru.pisarev.tm.controller.ProjectController;
import ru.pisarev.tm.controller.TaskController;
import ru.pisarev.tm.repository.CommandRepository;
import ru.pisarev.tm.repository.ProjectRepository;
import ru.pisarev.tm.repository.TaskRepository;
import ru.pisarev.tm.service.CommandService;
import ru.pisarev.tm.service.ProjectService;
import ru.pisarev.tm.service.TaskService;
import ru.pisarev.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    public void start(String... args) {
        displayWelcome();
        if (runArgs(args)) commandController.exit();
        process();
    }

    private boolean runArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        run(args[0]);
        return true;
    }

    private void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private void run(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.CMD_HELP:
            case ArgumentConst.ARG_HELP:
                commandController.displayHelp();
                break;
            case TerminalConst.CMD_VERSION:
            case ArgumentConst.ARG_VERSION:
                commandController.displayVersion();
                break;
            case TerminalConst.CMD_ABOUT:
            case ArgumentConst.ARG_ABOUT:
                commandController.displayAbout();
                break;
            case TerminalConst.CMD_INFO:
            case ArgumentConst.ARG_INFO:
                commandController.displayInfo();
                break;
            case TerminalConst.CMD_COMMANDS:
                commandController.displayCommands();
                break;
            case TerminalConst.CMD_ARGUMENTS:
                commandController.displayArguments();
                break;
            case TerminalConst.CMD_TASK_CLEAR:
                taskController.clear();
                break;
            case TerminalConst.CMD_TASK_CREATE:
                taskController.create();
                break;
            case TerminalConst.CMD_TASK_LIST:
                taskController.showList();
                break;
            case TerminalConst.CMD_TASK_SHOW_BY_ID:
                taskController.showById();
                break;
            case TerminalConst.CMD_TASK_SHOW_BY_INDEX:
                taskController.showByIndex();
                break;
            case TerminalConst.CMD_TASK_SHOW_BY_NAME:
                taskController.showByName();
                break;
            case TerminalConst.CMD_TASK_REMOVE_BY_ID:
                taskController.removeById();
                break;
            case TerminalConst.CMD_TASK_REMOVE_BY_INDEX:
                taskController.removeByIndex();
                break;
            case TerminalConst.CMD_TASK_REMOVE_BY_NAME:
                taskController.removeByName();
                break;
            case TerminalConst.CMD_TASK_UPDATE_BY_ID:
                taskController.updateById();
                break;
            case TerminalConst.CMD_TASK_UPDATE_BY_INDEX:
                taskController.updateByIndex();
                break;
            case TerminalConst.CMD_TASK_START_BY_ID:
                taskController.startById();
                break;
            case TerminalConst.CMD_TASK_START_BY_INDEX:
                taskController.startByIndex();
                break;
            case TerminalConst.CMD_TASK_START_BY_NAME:
                taskController.startByName();
                break;
            case TerminalConst.CMD_TASK_FINISH_BY_ID:
                taskController.finishById();
                break;
            case TerminalConst.CMD_TASK_FINISH_BY_INDEX:
                taskController.finishByIndex();
                break;
            case TerminalConst.CMD_TASK_FINISH_BY_NAME:
                taskController.finishByName();
                break;
            case TerminalConst.CMD_PROJECT_CLEAR:
                projectController.clear();
                break;
            case TerminalConst.CMD_PROJECT_CREATE:
                projectController.create();
                break;
            case TerminalConst.CMD_PROJECT_LIST:
                projectController.showList();
                break;
            case TerminalConst.CMD_PROJECT_SHOW_BY_ID:
                projectController.showById();
                break;
            case TerminalConst.CMD_PROJECT_SHOW_BY_INDEX:
                projectController.showByIndex();
                break;
            case TerminalConst.CMD_PROJECT_SHOW_BY_NAME:
                projectController.showByName();
                break;
            case TerminalConst.CMD_PROJECT_REMOVE_BY_ID:
                projectController.removeById();
                break;
            case TerminalConst.CMD_PROJECT_REMOVE_BY_INDEX:
                projectController.removeByIndex();
                break;
            case TerminalConst.CMD_PROJECT_REMOVE_BY_NAME:
                projectController.removeByName();
                break;
            case TerminalConst.CMD_PROJECT_UPDATE_BY_ID:
                projectController.updateById();
                break;
            case TerminalConst.CMD_PROJECT_UPDATE_BY_INDEX:
                projectController.updateByIndex();
                break;
            case TerminalConst.CMD_PROJECT_START_BY_ID:
                projectController.startById();
                break;
            case TerminalConst.CMD_PROJECT_START_BY_INDEX:
                projectController.startByIndex();
                break;
            case TerminalConst.CMD_PROJECT_START_BY_NAME:
                projectController.startByName();
                break;
            case TerminalConst.CMD_PROJECT_FINISH_BY_ID:
                projectController.finishById();
                break;
            case TerminalConst.CMD_PROJECT_FINISH_BY_INDEX:
                projectController.finishByIndex();
                break;
            case TerminalConst.CMD_PROJECT_FINISH_BY_NAME:
                projectController.finishByName();
                break;
            case TerminalConst.CMD_EXIT:
                commandController.exit();
            default:
                displayError();
        }
    }

    private void process() {
        String command = "";
        while (!TerminalConst.CMD_EXIT.equals(command)) {
            commandController.displayWait();
            command = TerminalUtil.nextLine();
            run(command);
        }
    }

    private void displayError() {
        System.out.println("Incorrect command. Use " + TerminalConst.CMD_HELP + " for display list of terminal commands.");
    }

}

package ru.pisarev.tm.api;

import ru.pisarev.tm.model.Task;

public interface ITaskController {

    void showList();

    void showById();

    void showByIndex();

    void showByName();

    void create();

    Task add(String name, String description);

    void clear();

    void removeById();

    void removeByIndex();

    void removeByName();

    void updateByIndex();

    void updateById();

    void startById();

    void startByIndex();

    void startByName();

    void finishById();

    void finishByIndex();

    void finishByName();

}
